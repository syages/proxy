//! Libraries
node_cryptojs = require('node-cryptojs-aes')
libColors = require("colors")
libCryptoJS = node_cryptojs.CryptoJS;
libFs = require("fs")
libHttp = require("http")
libHttps = require("https")
libRequest = require("request")
libQueryString = require("querystring")
libUrl = require("url")

//! Custom logic
var getTokenLogic = function(req, res, logicConf)
{
	var query = libQueryString.parse(libUrl.parse(req.url).query);
	// console.log(query);
	
	libRequest[req.method.toLowerCase()]({
		method: logicConf.method,
		url: (config.target + logicConf.to + "?grant_type=password&client_id="+config.consumer.key + "&username="+encodeURIComponent(query.username) + "&password="+query.password)
	}, function (error, response, body) {
		if (error) {
			console.err("Error: ".red + error);
			res.statusCode(500);
			res.end();
			return;
		}
		
		if (response.statusCode >= 200 && response.statusCode < 300)
		{
			res.writeHead(204, {
				"Authorization": crypt(body)
			});
			res.end();
		} else {
			res.writeHead(response.statusCode, {
				"Content-Type": "application/json"
			});
			res.write(body);
			res.end();
		}
	});
}

var config = {
	consumer: {
		key: "myClientKey",
		secret: "myClientSecret"
	},
	cipheringIV: "a2xhcgAAAAAAAAAA",
	cipheringKey: "vgZVTd6pxVHStLOf4ZMFdg6V92T1Ajbx5ol03/vx", // openssl rand -base64 30
	debug: true,
	https: {
		host: "localhost",
		key: libFs.readFileSync("key.pem"),
		cert: libFs.readFileSync("cert.pem")
	},
	proxy: {
		httpOn: 8080,
		httpTo: 80,
		httpsOn: 8443,
		httpsTo: 8443
	},
	routes: [
		{
			consumer: [true, false],
			customLogic: getTokenLogic,
			from: "/api/1/oauth/token",
			method: "GET",
			to: "/api/1/oauth/token"
		}
	],
	target: "http://127.0.0.1:9000"
}

//! Functions
var decrypt = function(encryptdata) {
	var decrypted = libCryptoJS.AES.decrypt(encryptdata, config.cipheringKey);
	decrypted = libCryptoJS.enc.Utf8.stringify(decrypted);
	return decrypted;
}
 
var crypt = function(cleardata) {
    var encrypted = libCryptoJS.AES.encrypt(cleardata, config.cipheringKey);
	return encrypted.toString();
}

function defaultTo(obj, key, val)
{
	if (obj[key] === null || obj[key] === undefined)
	{
		obj[key] = val;
	}
}

//! Proxy
libHttps.createServer(config.https, function (req, res) {
	var reqUrl = libUrl.parse(req.url);
	// console.log(reqUrl);
	
	// Looking for custom logic
	var customLogicConfig = undefined;
	config.routes.forEach(function(el, idx, arr) {
		if (el.from === reqUrl.pathname && el.method.toLowerCase() == req.method.toLowerCase())
		{
			customLogicConfig = el;
		}
	});
	
	// No custom logic found
	if (customLogicConfig === undefined) {
		// Proxying with undecrypted authorization
		if (req.headers.authorization !== null && req.headers.authorization !== undefined && req.headers.authorization.length != 0 && req.headers.authorization != "null")
		{
			if (config.debug == true) console.log(req.method.green + " " + reqUrl.pathname.white);
			
			var decryptedAuthorization = decrypt(req.headers.authorization);
			var auth = JSON.parse(decryptedAuthorization);
			
			delete req.headers.authorization;
			req.headers["Authorization"] = auth.token_type + " " + auth.access_token;
		} else {
			if (config.debug == true) console.log(req.method.cyan + " " + reqUrl.pathname.white);
		}
		
		var x = libRequest(config.target + req.url);
		req.pipe(x);
		x.pipe(res);
	}
	// Applying the custom logic we found
	else {
		if (config.debug == true) console.log(req.method.magenta + " " + reqUrl.pathname.white);
		customLogicConfig.customLogic(req, res, customLogicConfig);
	}
}).listen(config.proxy.httpsOn);

libHttp.createServer(function (req, res) {
	res.writeHead(301, {
		"Location": ("https://"+config.https.host+":"+config.proxy.httpsTo+req.url)
	});
	res.end();
}).listen(config.proxy.httpOn);